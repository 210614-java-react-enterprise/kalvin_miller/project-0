package dev.miller.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {

    private static Connection connection;
    private static final boolean IS_TEST = Boolean.parseBoolean(System.getenv("TEST"));

    public static Connection getConnection() throws SQLException {

        if(connection==null || connection.isClosed()){
            /*
            jdbc connection string format: jdbc:postgresql://host:port/database
            optionally if we're accessing a different schema: jdbc:postgresql://host:port/database?currentSchema=schemaName
             */
            String url = "jdbc:postgresql://training-db.ckwtvs9scvxl.us-east-2.rds.amazonaws" +
                    ".com:5432/postgres?currentSchema=project0";
            final String PASSWORD = "80207kjm";
            final String USERNAME = "postgres";
            connection = DriverManager.getConnection(url, USERNAME, PASSWORD);

        }
        //        System.out.println(connection.getMetaData().getDriverName());
        return connection;
    }
}

