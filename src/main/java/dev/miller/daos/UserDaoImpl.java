package dev.miller.daos;

import dev.miller.modules.BankAccount;
import dev.miller.modules.User;
import dev.miller.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl {

    public static List<User> getUserList() {
        String sql = "select id, username, password_str from bank_user";
        try (Connection connection = ConnectionUtil.getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sql);) {
            List<User> userList= new ArrayList<>();
            while(rs.next()){
                String username = rs.getString("username");
                User nextUser = new User();
                nextUser.setUsername(username);
                nextUser.setId(rs.getInt("id"));
                nextUser.setPassword(rs.getString("password_str"));
                userList.add(nextUser);
            }
            return userList;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static User addNewUser(User newUser) {
        BankAccount newAccount = new BankAccount(0);
        newAccount = BankAccountDaoImpl.addNewAccount(newAccount);

        String sql = " {call add_user(?, ?, ?) } ";

        try(Connection connection = ConnectionUtil.getConnection();
            CallableStatement callableStatement = connection.prepareCall(sql))
        {
            callableStatement.setString(1, newUser.getUsername());
            callableStatement.setObject(2, newUser.getPassword());



            if(newAccount != null) {
                callableStatement.setInt(3, newAccount.getAccountId());
            } else {
                callableStatement.setInt(3, 0);
            }

            callableStatement.execute();

            ResultSet resultSet = callableStatement.getResultSet();

            if(resultSet.next()){
                newUser.setId(resultSet.getInt("id"));


                newUser.setThisBankAccount(newAccount);

                String accountIdInputSQL = "update bank_user set account_id = " +
                        newUser.getThisBankAccount().getAccountId() +
                        "where id = " + newUser.getId();



                return newUser;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }


}
