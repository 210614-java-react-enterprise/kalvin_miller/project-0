package dev.miller.daos;

import dev.miller.modules.BankAccount;
import dev.miller.modules.User;
import dev.miller.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BankAccountDaoImpl {

    public static BankAccount getAccountFromId(int accountID) {
        String sql = "select account_id, balance from bank_account where account_id = " + accountID;
        try (Connection connection = ConnectionUtil.getConnection();
             Statement s = connection.createStatement();
             ResultSet resultSet = s.executeQuery(sql);) {
            BankAccount targetAccount = new BankAccount();
            if(resultSet.next()){
                targetAccount.setAccountId(resultSet.getInt("account_id"));
                targetAccount.setBalance(resultSet.getDouble("balance"));
            }
            return targetAccount;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static BankAccount depositFunds(BankAccount bankAccount, double depositAmount){

        double newBalance = bankAccount.getBalance() + depositAmount;
        String sql = "update bank_account set balance = " + newBalance +
                "where account_id = " + bankAccount.getAccountId();

        try (Connection connection = ConnectionUtil.getConnection();
             Statement s = connection.createStatement();) {

            s.executeUpdate(sql);
            bankAccount.setBalance(newBalance);

            return bankAccount;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static BankAccount withdrawFunds(BankAccount bankAccount, double withdrawAmount){

        double newBalance = bankAccount.getBalance() - withdrawAmount;
        String sql = "update bank_account set balance = " + newBalance +
                "where account_id = " + bankAccount.getAccountId();

        try (Connection connection = ConnectionUtil.getConnection();
             Statement s = connection.createStatement();) {

            s.executeUpdate(sql);
            bankAccount.setBalance(newBalance);

            return bankAccount;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static BankAccount addNewAccount(BankAccount newAccount) {
        String sql = " {call add_account(?) } ";

        try(Connection connection = ConnectionUtil.getConnection();
            CallableStatement callableStatement = connection.prepareCall(sql)){
            callableStatement.setDouble(1, newAccount.getBalance());
            callableStatement.execute();

            ResultSet resultSet = callableStatement.getResultSet();

            if(resultSet.next()){
                newAccount.setAccountId(resultSet.getInt("account_id"));
                return newAccount;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }
}
