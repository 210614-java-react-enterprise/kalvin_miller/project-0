package dev.miller.pages;

import dev.miller.modules.*;

import java.util.List;
import java.util.Scanner;

public class WelcomePage {

    public List<User> userList;

    public WelcomePage(List<User> userList){
        this.userList = userList;
    }

    public WelcomePage(){

    }


    public boolean render(){
        LoginPage loginPage = new LoginPage(this.userList);
        RegisterNewUserPage registerNewUserPage = new RegisterNewUserPage(this.userList);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome.");
        System.out.println("To login, input 'login'.");
        System.out.println("To register a new account, input 'register'");
        System.out.println("To exit the application, input 'exit'");

        String userInput = scanner.nextLine();

        switch (userInput) {
            case "login":
                loginPage.render();
                return true;
            case "register":
                registerNewUserPage.render();
                return true;
            case "exit":
                return true;
            default:
                System.out.println("Sorry, that was not a valid input.");
                WelcomePage welcomePage = new WelcomePage(this.userList);
                welcomePage.render();
                return false;
        }
    }
}
