package dev.miller.pages;

import dev.miller.daos.UserDaoImpl;
import dev.miller.modules.User;

import java.util.List;
import java.util.Scanner;

public class RegisterNewUserPage {

    public List<User> userList;

    public RegisterNewUserPage(List<User> userList) {
        this.userList = userList;
    }

    public boolean render(){


        Scanner scanner = new Scanner(System.in);
        System.out.println("Please input a username and password");
        System.out.print("Username: ");
        String username = scanner.nextLine();
        System.out.print("Password: ");
        String password = scanner.nextLine();

        User newUser = new User(username, password);

        newUser = UserDaoImpl.addNewUser(newUser);



        if(newUser != null){
            this.userList = UserDaoImpl.getUserList();
            ViewAccountPage accountPage = new ViewAccountPage(newUser.getThisBankAccount(), this.userList);
            accountPage.render();

            return true;
        } else return false;

    }
}
