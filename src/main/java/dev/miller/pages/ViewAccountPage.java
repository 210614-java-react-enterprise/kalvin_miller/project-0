package dev.miller.pages;

import dev.miller.daos.BankAccountDaoImpl;
import dev.miller.modules.BankAccount;
import dev.miller.modules.User;

import javax.swing.text.View;
import java.util.List;
import java.util.Scanner;

public class ViewAccountPage {



    private BankAccount bankAccount;
    private List<User> userList;

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public ViewAccountPage(BankAccount bankAccount, List<User> userList){
        this.bankAccount = bankAccount;
        this.userList = userList;
    }

    public boolean render(){

        System.out.println("Your bank account balance is: $" +
                String.format("%.2f",this.bankAccount.getBalance()));
        System.out.println("Input 'deposit' if you want to make a deposit.");
        System.out.println("Input 'withdraw' if you want to make a withdrawal");
        System.out.println("Input 'logout' if you want to log out.");

        Scanner scanner = new Scanner(System.in);

        String userInput = scanner.nextLine();

        switch (userInput) {
            case "deposit":
                ViewAccountPage nextInputPromptd = new ViewAccountPage(this.bankAccount, this.userList);

                System.out.println("Input how much money you would like to deposit: ");
                double depositAmount = scanner.nextDouble();

                if(depositAmount <= 0){
                    System.out.println("You can only deposit a positive amount");
                    nextInputPromptd.render();
                }

                this.bankAccount = BankAccountDaoImpl.depositFunds(this.bankAccount, depositAmount);
                if (this.bankAccount != null) {
                    System.out.println("You now have $" + String.format("%.2f",this.bankAccount.getBalance()) +
                            " in your account.");
                    nextInputPromptd.setBankAccount(this.bankAccount);
                }


                nextInputPromptd.render();
                break;
            case "withdraw":
                ViewAccountPage nextInputPromptw = new ViewAccountPage(this.bankAccount, this.userList);

                System.out.println("Input how much money you would like to withdraw: ");
                double withdrawAmount = scanner.nextDouble();

                if(withdrawAmount > this.bankAccount.getBalance()){
                    System.out.println("You cannot withdraw more than you have deposited");
                    nextInputPromptw.render();
                }

                this.bankAccount = BankAccountDaoImpl.withdrawFunds(this.bankAccount, withdrawAmount);
                if (this.bankAccount != null) {
                    System.out.println("You now have $" + String.format("%.2f",this.bankAccount.getBalance()) +
                            " in your account.");
                    nextInputPromptw.setBankAccount(this.bankAccount);
                }

                nextInputPromptw.render();

                break;
            case "logout":
                WelcomePage welcomePage = new WelcomePage(this.userList);
                welcomePage.render();

                break;
            default:
                System.out.println("Sorry, that isn't a valid input.");

                ViewAccountPage nextInputPrompt3 = new ViewAccountPage(this.bankAccount, this.userList);
                nextInputPrompt3.render();

                break;
        }

        return false;
    }
}
