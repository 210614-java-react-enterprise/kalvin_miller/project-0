package dev.miller.pages;

import dev.miller.daos.BankAccountDaoImpl;
import dev.miller.modules.BankAccount;
import dev.miller.modules.Terminal;
import dev.miller.modules.User;

import java.util.List;
import java.util.Scanner;

public class LoginPage {
    public List<User> userList;

    public LoginPage(List<User> userList) {
        this.userList = userList;
    }

    public boolean render(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please input a username and password");
        System.out.print("Username: ");
        String usernameInput = scanner.nextLine();
        System.out.print("Password: ");
        String passwordInput = scanner.nextLine();

        for(int i = 0; i < this.userList.size(); i++){
            if(usernameInput.equals(this.userList.get(i).getUsername()) &&
                    passwordInput.equals(this.userList.get(i).getPassword())){

                User loggedUser = this.userList.get(i);

                BankAccount loggedUserAccount = BankAccountDaoImpl.getAccountFromId(loggedUser.getId());

                loggedUser.setThisBankAccount(loggedUserAccount);

                ViewAccountPage accountPage =
                        new ViewAccountPage(loggedUserAccount, this.userList);

                accountPage.render();
                return true;

            }
        }

        System.out.println("Sorry, either the username or password is incorrect.");
        WelcomePage welcomePage = new WelcomePage(this.userList);
        welcomePage.render();

        return false;
    }
}
