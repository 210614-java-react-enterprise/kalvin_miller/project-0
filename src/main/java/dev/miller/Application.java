package dev.miller;

import dev.miller.daos.UserDaoImpl;
import dev.miller.pages.*;
import dev.miller.modules.*;

import java.util.*;

public class Application {



    public static void main(String[] args){
        List<User> userList = UserDaoImpl.getUserList();

        WelcomePage welcomePage = new WelcomePage(userList);

        welcomePage.render();

    }
}
