package dev.miller.modules;

public interface UserInterface {
    public boolean deposit(double depositAmount, BankAccount bankAccount);
    public double withdraw(double withdrawAmount, BankAccount bankAccount);
    public double viewBalance(BankAccount bankAccount);
}
