package dev.miller.modules;

public interface TerminalInterface {
    public User registerAccount(String username, String password);
    public boolean login(String username, String password);
    public void logout();
}
