package dev.miller.modules;

public class BankAccount implements BankAccountInterface{
    private int accountId;
    private double balance;

    public BankAccount(double initialAmount){
        this.balance = initialAmount;
    }

    public BankAccount(){
        this.balance = 0;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    //deposits funds, returns true if done successfully, returns false otherwise
    @Override
    public boolean depositFunds(double depositAmount) {
        if(depositAmount > 0){
            this.balance += depositAmount;
            return true;
        } else return false;
    }

    //withdraws funds, returns -1 if the withdraw would result in an overdraft
    @Override
    public double withdrawFunds(double withdrawAmount) {
        if (this.balance >= withdrawAmount) {
            this.balance -= withdrawAmount;
            return this.balance;
        } else return -1;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "accountId=" + accountId +
                ", balance=" + balance +
                '}';
    }

    @Override
    public double viewBalance() {
        return this.balance;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountID) {
        this.accountId = accountID;
    }
}
