package dev.miller.modules;

import java.util.List;

public class Terminal implements TerminalInterface{
    List<User> userList = null;
    User thisUser = null;

    public Terminal(List<User> userList){
        this.userList = userList;
    }

    Terminal(){
        super();
    }

    @Override
    public User registerAccount(String username, String password) {
        /* TODO: create a new user with the credentials provided, add it to the list of
        *    users, add it to the database */
        return null;
    }

    @Override
    public boolean login(String username, String password) {
        /* TODO: search through userList to see if any users match username and password
        *   set thisUser to the proper user or say we couldn't find what we needed*/
        return false;
    }

    @Override
    public void logout() {
        /* TODO: set thisUser to null */
    }

}
