package dev.miller.modules;

public interface BankAccountInterface {
    public boolean depositFunds(double depositAmount);
    public double withdrawFunds(double withdrawAmount);
    public double viewBalance();
}
