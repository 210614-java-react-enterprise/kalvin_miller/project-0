package dev.miller.modules;

import dev.miller.daos.UserDaoImpl;

public class User implements UserInterface{
    private String username;
    private String password;
    private int id;
    private BankAccount bankAccount;

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public User(){
        super();
    }

    public BankAccount getThisBankAccount() {
        return bankAccount;
    }

    public void setThisBankAccount(BankAccount thisBankAccount) {
        this.bankAccount = thisBankAccount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    /*
    public void addToDB() {
        User temp = UserDaoImpl.addNewUser(this);
        if(temp != null){
            this.id = temp.getId();
        }
    }
    */

    @Override
    public boolean deposit(double depositAmount, BankAccount bankAccount) {
        return false;
    }

    @Override
    public double withdraw(double withdrawAmount, BankAccount bankAccount) {
        return 0;
    }

    @Override
    public double viewBalance(BankAccount bankAccount) {
        return 0;
    }

    @Override
    public String toString() {
        if(this.bankAccount != null) {
            return "User{" +
                    "username='" + username + '\'' +
                    ", password='" + password + '\'' +
                    ", id=" + id + '\'' +
                    ", account= " + this.bankAccount.toString() +
                    '}';
        }
        else return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", id=" + id + '\'' +
                "NO ACCOUNT" +
                '}';
    }


}
