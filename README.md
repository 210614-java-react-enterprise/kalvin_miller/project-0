<h1>Project 0, Bank application</h1>

In order to execute this application, run the main function in the Application.java file. You can then follow the prompts to register an account, login to an account, deposit funds, withdraw funds, logout, and exit the application.

The different user input screens are represented by classes in this application, and each screen will prompt a different screen to execute whenever relevant(For example, the welcome screen will render the login screen)

The database for this project has two tables. A bank_user table and a bank_account table. Each bank_user has a bank_account id as a foreign key.

<h2>Classes:</h2> 

<h4>BankAccountDaoImpl:</h4> Used to access the database for functions related to bank accounts
<ul>
    <li>
    <b>public static BankAccount getAccountFromId(int accountID): </b>
        This method gets a bank account object from the table, using its id
    </li>
    <li>
    <b>public static BankAccount depositFunds(BankAccount bankAccount, double depositAmount): </b>
        This method adds funds to an accounts balance and updates the database
    </li>
    <li>
    <b>public static BankAccount withdrawFunds(BankAccount bankAccount, double withdrawAmount): </b>
        This method withdraws funds from an account's balance and updates the database
    </li>
    <li>
    <b>public static BankAccount addNewAccount(BankAccount newAccount): <b>
        This method adds a new bank account to the database
    </li>
</ul>

<h4>UserDaoImpl:</h4> Used to access the database for functions related to users
<ul>
    <li>
    <b>public static List<User> getUserList()</b>
        This method gets a list of all the users in the database and returns them as objects
    </li>
    <li>
    <b>public static User addNewUser(User newUser)</b>
        This method adds a new user to the database
    </li>
</ul>
    
<h4>BankAccount:</h4> The class that defines bank accounts and lets them be used in the API 
<ul>
    <li>
    <b>public boolean depositFunds(double depositAmount): </b>
        Changes the balance of a BankAccount object
    </li>
    <li>
    <b>public double withdrawFunds(double withdrawAmount): </b>
        Changes the balnace of a BankAccount object
    </li>
</ul>

<h4>User:</h4> The class that defines users and lets them be used in the API 

<h4>LoginPage:</h4> The class that renders the login screen
<ul>
    <li>
    <b>public boolean render(): </b>
        Prompts the user for a username and password, and tries to log them in
    </li>
</ul>

<h4>RegisterNewUserPage:</h4> The class that renders the registration screen
<ul>
    <li>
    <b>public boolean render(): </b>
        Prompts the user for a username and password, calls UserDaoImpl function to add them to the database and create their account.
    </li>
</ul>

<h4>ViewAccountPage:</h4> The class that renders the bank account screen
<ul>
    <li>
    <b>public booleanrender(): </b>
        Shows the user their balance and promts them to deposit or withdraw funds
    </li>
</ul>

<h4>WelcomPage:</h4> The class that renders the screen users see when not logged in
<ul>
    <li>
    <b>public boolean render(): </b>
        Prompts the user to either login or register an account
    </li>
</ul>

<h4>ConnectionUtil:</h4> The class that forms a connection to the database.
<ul>
    <li>
    <b>public static Connection getConnection() throws SQLException: </b>
        forms a connection with the database, used by DAOs
    </li>
</ul>
